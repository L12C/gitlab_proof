# Gitlab Proof

[Verifying my OpenPGP key: openpgp4fpr:0B0F50C17242F432721EDB45E5EEF4D45523C587]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs